## Installation

-- Install 32-bit version WAPP Stack from Bitnami.com
https://bitnami.com/stack/wapp/installer

(Requires local administrator access on a Windows server.)


-- Install Git
https://git-scm.com/download/win


-- Modify the php.ini to enable the extensions:
(find on these terms then remove the ; in front of the extension line)
pdo_odbc

   extension=php_pdo_odbc.dll

--Note: enable intl extension if experiencing database communication problems

   extension=php_intl.dll


-- Pull a copy of the repository into the apache2 folder.
The repository should replace the htdocs folder.


-- Using browser, login to the localhost/phppgadmin console and run the sql in DB_SQL_1.txt

--Note: default user\password is postgres\trombone

-- Connect to the website and create a company and user
localhost/companies/add
localhost/users/add


## Configuration

Read and edit `config/app.php` and setup the 'Datasources' and any other
configuration relevant for your application.

## Extra Info
http://phplens.com/lens/adodb/docs-adodb.htm

https://github.com/ADOdb/ADOdb