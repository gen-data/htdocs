CREATE SCHEMA cakephp;

CREATE SCHEMA exceptions;

CREATE TABLE cakephp.roles (
    id SERIAL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    description TEXT,
    created TIMESTAMP,
    modified TIMESTAMP
);

CREATE TABLE cakephp.companies (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE,
    profittool_name VARCHAR(5),
    path TEXT,
	ord_num INT,
    created TIMESTAMP,
    modified TIMESTAMP
);

CREATE TABLE cakephp.users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    profittool_un VARCHAR(4),
    email VARCHAR(255),
	mobile_id VARCHAR(255),
	mobile_time TIMESTAMP,
    role_id INT REFERENCES cakephp.roles(id),
    company_id INT REFERENCES cakephp.companies(id),
    created TIMESTAMP,
    modified TIMESTAMP,
    physical_inventory BOOLEAN,
    po_receipts BOOLEAN,
    signature_capture BOOLEAN,
    so_entry BOOLEAN,
    so_shipments BOOLEAN,
    transfers BOOLEAN
);

CREATE TABLE exceptions.so_headers (
    id SERIAL PRIMARY KEY,
    cust_num VARCHAR(255),
    cust_po_num VARCHAR(255),
    address1 VARCHAR(255),
    address2 VARCHAR(255),
    city VARCHAR(255),
    email VARCHAR(255),
    name VARCHAR(255),
    shipadd1 VARCHAR(255),
    shipadd2 VARCHAR(255),
    shipcity VARCHAR(255),
    shipcode VARCHAR(255),
    shipname VARCHAR(255),
    shipst VARCHAR(255),
    shipzip VARCHAR(255),
    state VARCHAR(255),
    zip VARCHAR(255),
    created TIMESTAMP
);

CREATE TABLE exceptions.so_details (
    so_header_id INT PRIMARY KEY REFERENCES exceptions.so_headers(id) ON UPDATE CASCADE ON DELETE CASCADE,
    item_num VARCHAR(255),
    item_qty VARCHAR(255),
    notes VARCHAR(255),
    price VARCHAR(255)
);

CREATE TABLE exceptions.in_physst (
    id SERIAL PRIMARY KEY,
    itemnum VARCHAR(255),
    whcode VARCHAR(255),
    binloc VARCHAR(255),
    userid VARCHAR(255),
    serlotnum VARCHAR(255),
    serlot VARCHAR(255),
    physcount INT
);