<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SoHeadersFixture
 *
 */
class SoHeadersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'cust_num' => ['type' => 'string', 'fixed' => true, 'length' => 255, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'cust_po_num' => ['type' => 'string', 'fixed' => true, 'length' => 255, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'cust_num' => 'Lorem ipsum dolor sit amet',
            'cust_po_num' => 'Lorem ipsum dolor sit amet',
            'created' => 1434380285
        ],
    ];
}
