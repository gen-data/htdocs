<?php
namespace App\Model\Table;

use App\Model\Entity\SoDelivery;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SoDeliveries Model
 *
 */
class SoDeliveriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('so_deliveries');
        $this->displayField('name');
        $this->primaryKey('id');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('date_signed');

        $validator
            ->requirePresence('delivery_num', 'create')
            ->notEmpty('delivery_num');

        $validator
            ->allowEmpty('note');

        $validator
            ->allowEmpty('name');

        $validator
            ->requirePresence('signature', 'create')
            ->notEmpty('signature');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'exceptions';
    }
}
