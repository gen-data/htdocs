<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('users');
        $this->displayField('username');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id'
        ]);
        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username');
            
        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');
            
        $validator
            ->allowEmpty('profittool_un');
            
        $validator
            ->add('email', 'valid', ['rule' => 'email'])
            ->allowEmpty('email');
            
        $validator
            ->add('physical_inventory', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('physical_inventory');
            
        $validator
            ->add('po_receipts', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('po_receipts');
            
        $validator
            ->add('signature_capture', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('signature_capture');
            
        $validator
            ->add('so_entry', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('so_entry');
            
        $validator
            ->add('so_shipments', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('so_shipments');
            
        $validator
            ->add('transfers', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('transfers');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        return $rules;
    }
}
