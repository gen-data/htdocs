<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    
    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }
    
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'profittool_un' => true,
        'email' => true,
        'role_id' => true,
        'company_id' => true,
        'physical_inventory' => true,
        'po_receipts' => true,
        'signature_capture' => true,
        'so_entry' => true,
        'so_shipments' => true,
        'transfers' => true,
        'bookmarks' => true,
    ];
}
