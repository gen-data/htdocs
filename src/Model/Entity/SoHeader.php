<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SoHeader Entity.
 */
class SoHeader extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'cust_num' => true,
        'cust_po_num' => true,
        'address1' => true,
        'address2' => true,
        'city' => true,
        'email' => true,
        'name' => true,
        'shipadd1' => true,
        'shipadd2' => true,
        'shipcity' => true,
        'shipcode' => true,
        'shipname' => true,
        'shipst' => true,
        'shipzip' => true,
        'state' => true,
        'zip' => true,
        'created' => true,
        'duedate' => true,        
        'so_details' => true,
    ];
}
