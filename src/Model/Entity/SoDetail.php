<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SoDetail Entity.
 */
class SoDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'so_header_id' => true,
        'item_num' => true,
        'item_qty' => true,
        'notes' => true,
        'price' => true,
        'so_header' => true,
    ];
}
