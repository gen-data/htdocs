<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InPhysst Entity.
 */
class InPhysst extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'itemnum' => true,
        'whcode' => true,
        'company' => true,
        'binloc' => true,
        'userid' => true,
        'serlotnum' => true,
        'serlot' => true,
        'physcount' => true,
    ];
}
