<?php
$this->assign('title', "Inventory Count");
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('User') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
    </ul>
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Submit Inventory Count'), ['action' => 'update']) ?></li>
        <li><?= $this->Html->link(__('Edit Inventory Count'), ['action' => 'edit', $inPhysst->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Inventory Count'), ['action' => 'delete', $inPhysst->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inPhysst->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Inventory Count'), ['action' => 'index']) ?> </li>
    </ul>
</div>
<div class="inPhysst view large-10 medium-9 columns">
    <h2><?= h($inPhysst->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Itemnum') ?></h6>
            <p><?= h($inPhysst->itemnum) ?></p>
            <h6 class="subheader"><?= __('Whcode') ?></h6>
            <p><?= h($inPhysst->whcode) ?></p>
            <h6 class="subheader"><?= __('Company') ?></h6>
            <p><?= h($inPhysst->company) ?></p>
            <h6 class="subheader"><?= __('Binloc') ?></h6>
            <p><?= h($inPhysst->binloc) ?></p>
            <h6 class="subheader"><?= __('Userid') ?></h6>
            <p><?= h($inPhysst->userid) ?></p>
            <h6 class="subheader"><?= __('Serlotnum') ?></h6>
            <p><?= h($inPhysst->serlotnum) ?></p>
            <h6 class="subheader"><?= __('Serlot') ?></h6>
            <p><?= h($inPhysst->serlot) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($inPhysst->id) ?></p>
            <h6 class="subheader"><?= __('Physcount') ?></h6>
            <p><?= $this->Number->format($inPhysst->physcount) ?></p>
        </div>
    </div>
</div>
