<?php
$this->assign('title', "Inventory Counts");
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('User') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
    </ul>
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Submit Inventory Counts'), ['action' => 'update_all']) ?></li>
    </ul>
</div>
<div class="inPhysst index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('itemnum') ?></th>
            <th><?= $this->Paginator->sort('whcode') ?></th>
            <th><?= $this->Paginator->sort('company') ?></th>
            <th><?= $this->Paginator->sort('binloc') ?></th>
            <th><?= $this->Paginator->sort('userid') ?></th>
            <th><?= $this->Paginator->sort('serlotnum') ?></th>
            <th><?= $this->Paginator->sort('physcount') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($inPhysst as $inPhysst): ?>
        <tr>
            <td><?= $this->Number->format($inPhysst->id) ?></td>
            <td><?= h($inPhysst->itemnum) ?></td>
            <td><?= h($inPhysst->whcode) ?></td>
            <td><?= h($inPhysst->company) ?></td>
            <td><?= h($inPhysst->binloc) ?></td>
            <td><?= h($inPhysst->userid) ?></td>
            <td><?= h($inPhysst->serlotnum) ?></td>
            <td><?= $this->Number->format($inPhysst->physcount) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $inPhysst->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $inPhysst->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $inPhysst->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inPhysst->id)]) ?>
                <?= $this->Form->postLink(__('Submit'), ['action' => 'update', $inPhysst->id], ['confirm' => __('Are you sure you want to submit # {0}?', $inPhysst->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
