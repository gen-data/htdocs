<?php
$this->assign('title', "Add A Inventory Count");
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('User') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
    </ul>
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Inventory Count'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="inPhysst form large-10 medium-9 columns">
    <?= $this->Form->create($inPhysst); ?>
    <fieldset>
        <legend><?= __('Add Inventory Count') ?></legend>
        <?php
            echo $this->Form->input('itemnum');
            echo $this->Form->input('whcode');
            echo $this->Form->input('company');
            echo $this->Form->input('binloc');
            echo $this->Form->input('userid');
            echo $this->Form->input('serlotnum');
            echo $this->Form->input('serlot');
            echo $this->Form->input('physcount');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
