<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $soDelivery->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $soDelivery->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List So Deliveries'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="soDeliveries form large-9 medium-8 columns content">
    <?= $this->Form->create($soDelivery) ?>
    <fieldset>
        <legend><?= __('Edit So Delivery') ?></legend>
        <?php
            echo $this->Form->input('date_signed');
            echo $this->Form->input('delivery_num');
            echo $this->Form->input('note');
            echo $this->Form->input('name');
            echo $this->Form->input('signature');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
