<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit So Delivery'), ['action' => 'edit', $soDelivery->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete So Delivery'), ['action' => 'delete', $soDelivery->id], ['confirm' => __('Are you sure you want to delete # {0}?', $soDelivery->id)]) ?> </li>
        <li><?= $this->Html->link(__('List So Deliveries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New So Delivery'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="soDeliveries view large-9 medium-8 columns content">
    <h3><?= h($soDelivery->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Delivery Num') ?></th>
            <td><?= h($soDelivery->delivery_num) ?></td>
        </tr>
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($soDelivery->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($soDelivery->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Signed') ?></th>
            <td><?= h($soDelivery->date_signed) ?></tr>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Note') ?></h4>
        <?= $this->Text->autoParagraph(h($soDelivery->note)); ?>
    </div>
    <div class="row">
        <h4><?= __('Signature') ?></h4>
        <?= $this->Text->autoParagraph(h($soDelivery->signature)); ?>
    </div>
</div>
