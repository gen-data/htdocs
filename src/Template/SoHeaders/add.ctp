<?php
$this->assign('title', "Add A Sales Order Header");
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
        <li><?= $this->Html->link(__('List Sales Order Headers'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="soHeaders form large-10 medium-9 columns">
    <?= $this->Form->create($soHeader); ?>
    <fieldset>
        <legend><?= __('Add So Header') ?></legend>
        <?php
            echo $this->Form->input('cust_num');
            echo $this->Form->input('cust_po_num');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
