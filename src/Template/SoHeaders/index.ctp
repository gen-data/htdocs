<?php
$this->assign('title', "Sales Order Headers");
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('User') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
    </ul>
</div>
<div class="soHeaders index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('cust_num') ?></th>
            <th><?= $this->Paginator->sort('cust_po_num') ?></th>
            <th><?= $this->Paginator->sort('shipcode') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($soHeaders as $soHeader): ?>
        <tr>
            <td><?= $this->Number->format($soHeader->id) ?></td>
            <td><?= h($soHeader->cust_num) ?></td>
            <td><?= h($soHeader->cust_po_num) ?></td>
            <td><?= h($soHeader->shipcode) ?></td>
            <td><?= h($soHeader->created) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $soHeader->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $soHeader->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $soHeader->id], ['confirm' => __('Are you sure you want to delete # {0}?', $soHeader->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
