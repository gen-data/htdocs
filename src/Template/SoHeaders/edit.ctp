<?php
$this->assign('title', "Edit Sales Order Header");
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('User') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
    </ul>
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $soHeader->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $soHeader->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sales Order Headers'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="soHeaders form large-10 medium-9 columns">
    <?= $this->Form->create($soHeader); ?>
    <fieldset>
        <legend><?= __('Edit So Header') ?></legend>
        <?php
            echo $this->Form->input('cust_num');
            echo $this->Form->input('cust_po_num');
            echo $this->Form->input('address1');
            echo $this->Form->input('address2');
            echo $this->Form->input('city');
            echo $this->Form->input('email');
            echo $this->Form->input('name');
            echo $this->Form->input('shipadd1');
            echo $this->Form->input('shipadd2');
            echo $this->Form->input('shipcity');
            echo $this->Form->input('shipcode');
            echo $this->Form->input('shipname');
            echo $this->Form->input('shipst');
            echo $this->Form->input('shipzip');
            echo $this->Form->input('state');
            echo $this->Form->input('zip');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
