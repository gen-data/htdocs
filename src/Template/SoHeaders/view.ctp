<?php
$this->assign('title', "Sales Order Header");
?>
<div class="actions columns large-2 medium-3">
    <h3><?= __('User') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
    </ul>
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Resubmit Sales Order'), ['action' => 'resubmit_sales_order', $soHeader->id]) ?> </li>
        <li><?= $this->Html->link(__('Edit Sales Order Header'), ['action' => 'edit', $soHeader->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sales Order Header'), ['action' => 'delete', $soHeader->id], ['confirm' => __('Are you sure you want to delete # {0}?', $soHeader->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sales Order Headers'), ['action' => 'index']) ?> </li>
    </ul>
</div>
<div class="soHeaders view large-10 medium-9 columns">
    <h2><?= h($soHeader->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Customer Number') ?></h6>
            <?php if ($custNumValid) { ?>
                <p><?= h($soHeader->cust_num) ?></p>
            <?php } else { ?>
                <p><font color="red"><?= h($soHeader->cust_num) ?></font></p>
            <?php } ?>
            <h6 class="subheader"><?= __('Customer Purchase Order Number') ?></h6>
            <p><?= h($soHeader->cust_po_num) ?></p>
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($soHeader->name) ?></p>
            <h6 class="subheader"><?= __('Email') ?></h6>
            <p><?= h($soHeader->email) ?></p>
            <h6 class="subheader"><?= __('Address 1') ?></h6>
            <p><?= h($soHeader->address1) ?></p>
            <h6 class="subheader"><?= __('Address 2') ?></h6>
            <p><?= h($soHeader->address2) ?></p>
            <h6 class="subheader"><?= __('City') ?></h6>
            <p><?= h($soHeader->city) ?></p>
            <h6 class="subheader"><?= __('State') ?></h6>
            <p><?= h($soHeader->state) ?></p>
            <h6 class="subheader"><?= __('Zip') ?></h6>
            <p><?= h($soHeader->zip) ?></p>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Ship-to Code') ?></h6>
			<?php if ($shipToNumValid) { ?>
                <p><?= h($soHeader->shipcode) ?></p>
            <?php } else { ?>
                <p><font color="red"><?= h($soHeader->shipcode) ?></font></p>
            <?php } ?>
            <h6 class="subheader"><?= __('Ship-to Address 1') ?></h6>
            <p><?= h($soHeader->shipadd1) ?></p>
            <h6 class="subheader"><?= __('Ship-to Address 2') ?></h6>
            <p><?= h($soHeader->shipadd2) ?></p>
            <h6 class="subheader"><?= __('Ship-to City') ?></h6>
            <p><?= h($soHeader->shipcity) ?></p>
            <h6 class="subheader"><?= __('Ship-to Name') ?></h6>
            <p><?= h($soHeader->shipname) ?></p>
            <h6 class="subheader"><?= __('Ship-to State') ?></h6>
            <p><?= h($soHeader->shipst) ?></p>
            <h6 class="subheader"><?= __('Ship-to Zip') ?></h6>
            <p><?= h($soHeader->shipzip) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Sales Order Details') ?></h4>
    <?php if (!empty($soHeader->so_details)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Item Number') ?></th>
            <th><?= __('Item Quantity') ?></th>
            <th><?= __('Price') ?></th>
            <th><?= __('Notes') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($soHeader->so_details as $soDetails): ?>
        <tr>
            <?php foreach ($validItemArray as $itemNum => $valid)
            { if($itemNum == $soDetails->item_num) { if($valid){ ?>
                <td> <?= h($soDetails->item_num) ?></td>
            <?php } else { ?>
                <td><font color="red"><?= h($soDetails->item_num) ?></font></td>
            <?php }}} ?>
            <td><?= h($soDetails->item_qty) ?></td>
            <td><?= h($soDetails->price) ?></td>
            <td><?= h($soDetails->notes) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'SoDetails', 'action' => 'view', $soDetails->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'SoDetails', 'action' => 'edit', $soDetails->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'SoDetails', 'action' => 'delete', $soDetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $soDetails->so_header_id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
