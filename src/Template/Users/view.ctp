<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="users view large-10 medium-9 columns">
    <h2><?= h($user->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Username') ?></h6>
            <p><?= h($user->username) ?></p>
            <h6 class="subheader"><?= __('Password') ?></h6>
            <p><?= h($user->password) ?></p>
            <h6 class="subheader"><?= __('Profittool Un') ?></h6>
            <p><?= h($user->profittool_un) ?></p>
            <h6 class="subheader"><?= __('Email') ?></h6>
            <p><?= h($user->email) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($user->id) ?></p>
            <h6 class="subheader"><?= __('Role Id') ?></h6>
            <p><?= $this->Number->format($user->role_id) ?></p>
            <h6 class="subheader"><?= __('Company Id') ?></h6>
            <p><?= $this->Number->format($user->company_id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($user->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($user->modified) ?></p>
        </div>
        <div class="large-2 columns booleans end">
            <h6 class="subheader"><?= __('Physical Inventory') ?></h6>
            <p><?= $user->physical_inventory ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Po Receipts') ?></h6>
            <p><?= $user->po_receipts ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Signature Capture') ?></h6>
            <p><?= $user->signature_capture ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('So Entry') ?></h6>
            <p><?= $user->so_entry ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('So Shipments') ?></h6>
            <p><?= $user->so_shipments ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Transfers') ?></h6>
            <p><?= $user->transfers ? __('Yes') : __('No'); ?></p>
        </div>
    </div>
</div>
