<?php
$this->assign('title', "Home");
?>

<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
    </ul>
    <?php if ($userLevel === 1) { ?>
    <h3><?php echo __('Site Management'); ?></h3>
    <ul class="side-nav">
        <li><?php echo $this->Html->link(__('Manage Companies'), '/Companies'); ?></li>
        <li><?php echo $this->Html->link(__('Manage Users'), '/Users'); ?> </li>
        <li><?php echo $this->Html->link(__('Manage Roles'), '/Roles'); ?> </li>
    </ul>
    <?php } ?>
</div>
<div class="soDetails index large-10 medium-9 columns">
    <h2><?php echo "ProfitTool Mobile Management" ?></h2>
    <br>
    <div class="row">
        <div class="columns large-12">
        <ul>
        <?php if ($soExceptions > 0) { ?>
            <p><font color="red">There are currently <?php echo $soExceptions ?> Sales Order exceptions.</font></p>
            <?php echo $this->Html->link(__('Manage Sales Order Exceptions'), '/SoHeaders'); ?>
        <?php } else { ?>
            <p>There are currently <?php echo $soExceptions ?> Sales Order exceptions.</p>
        <?php } ?>
            <hr/>
        <?php if ($countExceptions > 0) { ?>
            <p><font color="red">There are currently <?php echo $countExceptions ?> Inventory counts.</font></p>
            <?php echo $this->Html->link(__('Manage Inventory Count'), '/InPhysst'); ?>
        <?php } else { ?>
            <p>There are currently <?php echo $countExceptions ?> Inventory counts.</p>
        <?php } ?>
            <hr/>
        <?php if ($deliveryExceptions > 0) { ?>
            <p><font color="red">There are currently <?php echo $deliveryExceptions ?> Delivery exceptions.</font></p>
            <?php echo $this->Html->link(__('Manage Deliveries'), '/SoDeliveries'); ?>
        <?php } else { ?>
            <p>There are currently <?php echo $deliveryExceptions ?> Delivery exceptions.</p>
        <?php } ?>
        </ul>
        </div>    
    </div>
</div>