<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display']) ?> </li>
        <?php if (!$authUser) { ?>
                <li><?php echo $this->Html->link(__('Login'), '/Users/login'); ?></li>
        <?php } else { ?>
                <li><?php echo $this->Html->link(__('Logout'), '/Users/logout'); ?></li>
        <?php } ?>
        <li><?= $this->Html->link(__('Edit Company'), ['action' => 'edit', $company->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Company'), ['action' => 'delete', $company->id], ['confirm' => __('Are you sure you want to delete # {0}?', $company->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Companies'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="companies view large-10 medium-9 columns">
    <h2><?= h($company->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($company->name) ?></p>
            <h6 class="subheader"><?= __('Profittool Name') ?></h6>
            <p><?= h($company->profittool_name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($company->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($company->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($company->modified) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Path') ?></h6>
            <?= $this->Text->autoParagraph(h($company->path)); ?>

        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Tables') ?></h4>
    <?php if (!empty($company->tables)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Name') ?></th>
            <th><?= __('Path') ?></th>
            <th><?= __('Company Id') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($company->tables as $tables): ?>
        <tr>
            <td><?= h($tables->id) ?></td>
            <td><?= h($tables->name) ?></td>
            <td><?= h($tables->path) ?></td>
            <td><?= h($tables->company_id) ?></td>
            <td><?= h($tables->created) ?></td>
            <td><?= h($tables->modified) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Tables', 'action' => 'view', $tables->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Tables', 'action' => 'edit', $tables->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tables', 'action' => 'delete', $tables->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tables->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Users') ?></h4>
    <?php if (!empty($company->users)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Username') ?></th>
            <th><?= __('Password') ?></th>
            <th><?= __('Profittool Un') ?></th>
            <th><?= __('Email') ?></th>
            <th><?= __('Role Id') ?></th>
            <th><?= __('Company Id') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
            <th><?= __('Physical Inventory') ?></th>
            <th><?= __('Po Receipts') ?></th>
            <th><?= __('Signature Capture') ?></th>
            <th><?= __('So Entry') ?></th>
            <th><?= __('So Shipments') ?></th>
            <th><?= __('Transfers') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($company->users as $users): ?>
        <tr>
            <td><?= h($users->id) ?></td>
            <td><?= h($users->username) ?></td>
            <td><?= h($users->password) ?></td>
            <td><?= h($users->profittool_un) ?></td>
            <td><?= h($users->email) ?></td>
            <td><?= h($users->role_id) ?></td>
            <td><?= h($users->company_id) ?></td>
            <td><?= h($users->created) ?></td>
            <td><?= h($users->modified) ?></td>
            <td><?= h($users->physical_inventory) ?></td>
            <td><?= h($users->po_receipts) ?></td>
            <td><?= h($users->signature_capture) ?></td>
            <td><?= h($users->so_entry) ?></td>
            <td><?= h($users->so_shipments) ?></td>
            <td><?= h($users->transfers) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
