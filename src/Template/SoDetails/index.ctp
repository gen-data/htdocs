<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New So Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List So Headers'), ['controller' => 'SoHeaders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New So Header'), ['controller' => 'SoHeaders', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="soDetails index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('so_header_id') ?></th>
            <th><?= $this->Paginator->sort('item_num') ?></th>
            <th><?= $this->Paginator->sort('item_qty') ?></th>
            <th><?= $this->Paginator->sort('notes') ?></th>
            <th><?= $this->Paginator->sort('price') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($soDetails as $soDetail): ?>
        <tr>
            <td><?= $this->Number->format($soDetail->id) ?></td>
            <td>
                <?= $soDetail->has('so_header') ? $this->Html->link($soDetail->so_header->id, ['controller' => 'SoHeaders', 'action' => 'view', $soDetail->so_header->id]) : '' ?>
            </td>
            <td><?= h($soDetail->item_num) ?></td>
            <td><?= h($soDetail->item_qty) ?></td>
            <td><?= h($soDetail->notes) ?></td>
            <td><?= h($soDetail->price) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $soDetail->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $soDetail->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $soDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $soDetail->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
