<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $soDetail->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $soDetail->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List So Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List So Headers'), ['controller' => 'SoHeaders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New So Header'), ['controller' => 'SoHeaders', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="soDetails form large-10 medium-9 columns">
    <?= $this->Form->create($soDetail); ?>
    <fieldset>
        <legend><?= __('Edit So Detail') ?></legend>
        <?php
            echo $this->Form->input('so_header_id', ['options' => $soHeaders, 'empty' => true]);
            echo $this->Form->input('item_num');
            echo $this->Form->input('item_qty');
            echo $this->Form->input('notes');
            echo $this->Form->input('price');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
