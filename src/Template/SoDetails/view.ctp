<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit So Detail'), ['action' => 'edit', $soDetail->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete So Detail'), ['action' => 'delete', $soDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $soDetail->id)]) ?> </li>
        <li><?= $this->Html->link(__('List So Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New So Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List So Headers'), ['controller' => 'SoHeaders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New So Header'), ['controller' => 'SoHeaders', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="soDetails view large-10 medium-9 columns">
    <h2><?= h($soDetail->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('So Header') ?></h6>
            <p><?= $soDetail->has('so_header') ? $this->Html->link($soDetail->so_header->id, ['controller' => 'SoHeaders', 'action' => 'view', $soDetail->so_header->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Item Num') ?></h6>
            <?php if ($itemNumValid) { ?>
                <p><?= h($soDetail->item_num) ?></p>
            <?php } else { ?>
                <p><font color="red"><?= h($soDetail->item_num) ?></font></p>
            <?php } ?>
            <h6 class="subheader"><?= __('Item Qty') ?></h6>
            <p><?= h($soDetail->item_qty) ?></p>
            <h6 class="subheader"><?= __('Notes') ?></h6>
            <p><?= h($soDetail->notes) ?></p>
            <h6 class="subheader"><?= __('Price') ?></h6>
            <p><?= h($soDetail->price) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($soDetail->id) ?></p>
        </div>
    </div>
</div>
