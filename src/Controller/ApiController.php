<?php
namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
/**
 * API Controller
 *
 * @property Initemmm $Initemmm
 * @property PaginatorComponent $Paginator
 */
class ApiController extends AppController
{

/**
 * Initialize Methods
 */

    public $uses = array('User');

    function blackhole()
    {
        if(!isset($_SERVER['HTTPS']))
        {
            $this->redirect('https://' . env('SERVER_NAME') . $this->here);
        }
    }

    public function initialize()
    {
        $this->loadComponent('Auth', [
            'authorize'=> 'Controller',
            'authenticate' => [
                'Basic'
            ],
            'unauthorizedRedirect' => false
        ],
                    'RequestHandler');
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        $this->loadModel('Users');
        $this->autoRender = FALSE;

//            $this->Security->blackHoleCallback = 'blackhole';
//            $this->Security->requireSecure();
    }

    public function isAuthorized($user)
    {
        // $newUUID = $this->request->header('UUID');
//         $oldUUID = $user['mobile_id'];
//         $dbtime = new Time($user['mobile_time']);
//         $now = Time::now();
//
//         $secondsNow = ($now->i18nFormat('HH')*60*60)+($now->i18nFormat('mm')*60)+($now->i18nFormat('ss'));
//         $secondsDBTime = ($dbtime->i18nFormat('HH')*60*60)+($dbtime->i18nFormat('mm')*60)+($dbtime->i18nFormat('ss'))-(20*60);
//
//         if ($newUUID !== "" || $oldUUID !== "")
//         {
//             if ($newUUID === $oldUUID)
//             {
//                 return true;
//             }
//             else
//             {
//                 if ($dbtime->isToday())
//                 {
//                     if ($secondsNow < $secondsDBTime)
//                     {
//                         throw new UnauthorizedException();
//                     }
//                     else
//                     {
//                         return true;
//                     }
//                 }
//                 else
//                 {
//                     return true;
//                 }
//             }
//         }
//         else
//         {
            return true;
//         }
    }

    public function index()
    {
        $this->redirect('/');
    }

/**
 * Postgres Methods
 */

    /**
     * Login API method
     *
     * @param null.
     * @return the users information
     * @throws \Cake\Network\Exception\UnauthorizedException When record not found.
     */
    public function json_login()
    {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->get($this->Auth->user('id'));
        $user->mobile_id = trim($this->request->header('UUID'));
        $user->mobile_time = date(DATE_ISO8601);
        $usersTable->save($user);

        $this->loadModel('Users');
        $result = $this->Users->get($this->Auth->user('id'),
            ['contain' => ['Companies', 'Roles']]
        );
        if ($result)
        {
            echo json_encode(array('User' => $result));
            $this->set("_serialize", array('User' => $result));
        }
        else
        {
            throw new UnauthorizedException();
        }
    }

    /**
     * Logout API method
     *
     * @param null.
     * @return null.
     */
    public function json_logout()
    {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->get($this->Auth->user('id'));
        $user->mobile_id = "";
        $user->mobile_time = NULL;
        $usersTable->save($user);
        $this->Auth->logout();
    }

/*
 * VFP Methods
 */
    public function json_item_one($id = null)
    {
        if ($this->request->is('get'))
        {
            $company = $this->Auth->user('company_id');
            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($company);
            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
            $dd = $this->retrieveDataDictionary("initemmm", $vfpConn);
            $select = $this->createSelectAllFromVfp($dd);
            $stmt = "SELECT $select FROM initemmm WHERE itemnum == \"$id\"";
            $result = $this->vfpExecuteSQL($vfpConn, $stmt);
            if ($result)
            {
                $return = array("itemExists" => TRUE, array("INITEMMM" => $result->GetRowAssoc()));
                $result->MoveNext();
            }
            else
            {
                $return = array("itemExists" => FALSE);
            }
            $this->set('return', $return);
            echo json_encode($return);
            $this->set('_serialize', 'return');
            $this->vfpCloseConnection($vfpConn);
        }
    }

    public function json_item_all()
    {
        if ($this->request->is('get'))
        {
            $company = $this->Auth->user('company_id');
            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($company);

            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
            $dd = $this->retrieveDataDictionary("initemmm", $vfpConn);
            $select = $this->createSelectAllFromVfp($dd);
            $return = $this->vfpExecuteSQL($vfpConn, "SELECT $select FROM initemmm");
            $array = [];
            if ($return)
            {
                $array[] = array("itemExists" => TRUE);
                while (!$return->EOF)
                {
                $array[] = array("INITEMMM" => $return->GetRowAssoc());
                $return->MoveNext();
                }
            }
            else
            {
                $array[] = array("itemExists" => FALSE);
            }
            echo json_encode($array);
            $this->set("_serialize", $array);
            $this->vfpCloseConnection($vfpConn);
        }
    }

    public function json_bin_one($item = null, $ware = null, $bin = null)
    {
        // limits this to http GET methods
        if ($this->request->is('get'))
        {
            // Finds the information for the user submitting this.
            $company = $this->Auth->user('company_id');
            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($company);

            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
            $dd = $this->retrieveDataDictionary("inwaredt", $vfpConn);
            $select = $this->createSelectAllFromVfp($dd);
            $return1 = $this->vfpExecuteSQL($vfpConn, "SELECT $select FROM inwaredt WHERE itemnum == \"$item\" AND whnum == \"$ware\" AND binloc == \"$bin\"");
            $return2 = $this->vfpExecuteSQL($vfpConn, "SELECT $select FROM inwaredt WHERE itemnum == \"$item\" AND whnum == \"$ware\" AND altbinloc == \"$bin\"");
            $return3 = $this->vfpExecuteSQL($vfpConn, "SELECT $select FROM inwaredt WHERE itemnum == \"$item\" AND whnum == \"$ware\" AND altbinloc2 == \"$bin\"");
            if (empty($return1) && empty($return2) && empty($return3))
            {
                $return = array("binExists" => FALSE);
                echo json_encode($return);
                $this->set("_serialize", array("binExists" => FALSE));
            }
            else
            {
                $return = array("binExists" => TRUE);
                echo json_encode($return);
                $this->set("_serialize", array("binExists" => TRUE));
            }
            $this->vfpCloseConnection($vfpConn);
        }
    }

    public function json_warehouse_one($ware = null)
    {
        if ($this->request->is('get'))
        {
            $company = $this->Auth->user('company_id');
            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($company);

            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
            $dd = $this->retrieveDataDictionary("inwaremm", $vfpConn);
            $select = $this->createSelectAllFromVfp($dd);
            $return1 = $this->vfpExecuteSQL($vfpConn, "SELECT $select FROM inwaremm WHERE whnum == \"$ware\"");
            if (empty($return1))
            {
                $return = array("warehouseExists" => FALSE);
                echo json_encode($return);
                $this->set("_serialize", array("warehouseExists" => FALSE));
            }
            else
            {
                $return = array("warehouseExists" => TRUE, array("INWAREMM" => $return1->GetRowAssoc()));
                echo json_encode($return);
                $this->set("_serialize", array("warehouseExists" => TRUE, array("INWAREMM" => $return1->GetRowAssoc())));
            }
            $this->vfpCloseConnection($vfpConn);
        }
    }

    public function json_warehouse_all()
    {
        if ($this->request->is('get'))
        {
            $company = $this->Auth->user('company_id');
            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($company);

            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
            $dd = $this->retrieveDataDictionary("inwaremm", $vfpConn);
            $select = $this->createSelectAllFromVfp($dd);
            $return = $this->vfpExecuteSQL($vfpConn, "SELECT $select FROM inwaremm");
            $array = [];
            if ($return)
            {
                while (!$return->EOF)
                {
                $array[] = array("INWAREMM" => $return->GetRowAssoc());
                $return->MoveNext();
                }
            }
            else
            {
                $array[] = array("warehouses" => FALSE);
            }
            echo json_encode($array);
            $this->set("_serialize", $array);
            $this->vfpCloseConnection($vfpConn);
        }
    }

    public function json_user_one($id = null)
    {
        if ($this->request->is('get'))
        {
            $company = $this->Auth->user('company_id');
            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($company);

            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
            $upperId = strtoupper($id);
            $return = $this->vfpExecuteSQL($vfpConn, "SELECT DISTINCT userid FROM Inphysst WHERE userid == \"$upperId\"");
            if ($return)
            {
                $this->layout = NULL;
                $return = array("userExists" => TRUE);
                echo($return);
                $this->set("_serialize", array("userExists" => TRUE));
            }
            else
            {
                $this->layout = NULL;
                $return = array("userExists" => FALSE);
                echo($return);
                $this->set("_serialize", array("userExists" => FALSE));
            }
            $this->vfpCloseConnection($vfpConn);
        }
    }

    public function json_user_all()
    {
        if ($this->request->is('get'))
        {
            $company = $this->Auth->user('company_id');
            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($company);

            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
            $return = $this->vfpExecuteSQL($vfpConn, "SELECT DISTINCT userid FROM Inphysst");
            $array = [];
            if ($return)
            {
                while (!$return->EOF)
                {
                    $array[] = array("INPHYSST" => $return->GetRowAssoc());
                    $return->MoveNext();
                }
            }
            else
            {
                $array[] = array("userExists" => FALSE);
            }
            echo json_encode($array);
            $this->set("_serialize", $array);
            $this->vfpCloseConnection($vfpConn);
        }
    }

    public function json_user_by_warehouse_one($ware = NULL)
    {
        if ($this->request->is('get'))
        {
            $company = $this->Auth->user('company_id');
            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($company);

            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
            $return = $this->vfpExecuteSQL($vfpConn, "SELECT DISTINCT userid FROM Inphysst WHERE whcode == \"$ware\"");
            $array = [];
            if ($return)
            {
                while (!$return->EOF)
                {
                    $array[] = array("INPHYSST" => $return->GetRowAssoc());
                    $return->MoveNext();
                }
            }
            else
            {
                $array[] = array("userExists" => FALSE);
            }
            echo json_encode($array);
            $this->set("_serialize", $array);
            $this->vfpCloseConnection($vfpConn);
        }
    }



    /**
     * Apple Push Notification Token Set API method
     *
     * @param APN token.
     * @return null.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function json_apn_token_set()
    {
        if ($this->request->is('post'))
        {
            $data = $this->request->input();

            $usersTable = TableRegistry::get('Users');
            $user = $usersTable->get($this->Auth->user('id'));
            $user->mobile_token = $data;
            $usersTable->save($user);
        }
    }

    /**
     * Sales Order Submit method
     *
     * @param Sales orders from ios Device.
     * @return number of sales orders submitted.
     * This will either create a tab delimited file in the windows system or will save it to the postgres database.
     */
    public function tdf_sales_order()
    {
        // Set the timeout time limit higher
        set_time_limit(5000);
        // Limits this to http POST methods and only can be submitted by Authorized users
        if ($this->request->is('post') && $this->Auth->user())
        {
            // sets the request data to $data
            $data = $this->request->input();
            // sets counter
            $ordersReceived = 0;
            // decodes the json
            $obj = json_decode($data, true);
            // checks if there are orders
            if ($obj['Orders'])
            {
                // loops throught the orders
                foreach ($obj['Orders'] as $ordArray)
                {
                    $ordersReceived++;
                    // gets information for the user submitting the orders
                    $company = $this->Auth->user('company_id');
                    $this->loadModel('Companies');
                    $companyInfo = $this->Companies->get($company);

                    $validHeaderOrder = TRUE;
                    // finds the customer in VFP database
                    $cust = $this->customer_one($ordArray['custNum']);

                    $ordnum = $companyInfo['ord_num'];
                    $ordnum++;
                    $companyInfo['ord_num'] = $ordnum;
                    $this->Companies->save($companyInfo);

                    // sets these for the tab delimited text
                    $detailText = "";
                    $headerText = "";

                    // makes sure the ship to code is valid
                    if ($ordArray['shipcode'] && !$this->ship_to_one($ordArray['shipcode'], trim($cust['NUMBER'])))
                    {
                            $validHeaderOrder = FALSE;
                    }
                    // makes sure the customer is set
                    if ($cust)
                    {
                        // creates the dictionary to later be deconstructed into the tab delimited text
                        $dueDate = Time::createFromFormat('Y-m-d', $ordArray['dueDate'])->format('m/d/Y');
                        $ord_ha = [
                            'webordnum' => trim($companyInfo['ord_num']),
                            'orddate' => "",
                            'duedate' => $dueDate,
                            'custponum' => trim($ordArray['custPONum']),
                            'email' => $ordArray['email'],
                            'name' => trim($cust['NAME']),
                            'address1' => trim($cust['ADDRESS1']),
                            'address2' => trim($cust['ADDRESS2']),
                            'city' => trim($cust['CITY']),
                            'state' => trim($cust['STATE']),
                            'zip' => trim($cust['ZIP']),
                            'shipname' => ($ordArray['shipcode'] ? "" : $ordArray['shipname']),
                            'shipadd1' => ($ordArray['shipcode'] ? "" : $ordArray['shipadd1']),
                            'shipadd2' => ($ordArray['shipcode'] ? "" : $ordArray['shipadd2']),
                            'shipcity' => ($ordArray['shipcode'] ? "" : $ordArray['shipcity']),
                            'shipst' => ($ordArray['shipcode'] ? "" : $ordArray['shipst']),
                            'shipzip' => ($ordArray['shipcode'] ? "" : $ordArray['shipzip']),
                            'number' => trim($cust['NUMBER']),
                            'shipcode' => ($ordArray['shipcode'] ?: ""),
                            'ordnum' => ""
                        ];
                    }
                    else
                    {
                        $validHeaderOrder = FALSE;
                    }

                    // checks if order is still valid and details are available
                    if ($ordArray['Details'] && $validHeaderOrder)
                    {
                        // sets header text to the deconstructed ord_ha dictionary
                        if ($headerText)
                        {
                            $headerText .= implode($ord_ha, "\t")."\r\n";
                        }
                        else
                        {
                            $headerText = implode($ord_ha, "\t")."\r\n";
                        }
                        foreach ($ordArray['Details'] as $detArray)
                        {
                            $validDetailOrder = TRUE;

                            // makes sure there is an item qty and item num and order valid
                            if ($detArray['itemQty'] && $detArray['itemNum'] && $validDetailOrder)
                            {
                                // finds item
                                $item = $this->item_one($detArray['itemNum']);
                                if (isset($item) && $validDetailOrder)
                                {
                                    // creates detail dictionary
                                    $ord_da = [
                                        'webordnum' => trim($companyInfo['ord_num']),
                                        'itemnum' => trim($item['ITEMNUM']),
                                        'notes' => trim($detArray['notes']),
                                        'price' => ($detArray['price'] ? $detArray['price'] : ""),
                                        'qtyord' => trim($detArray['itemQty']),
                                        'ordnum' => "",
                                        'pricemod' => ($detArray['price'] ? TRUE : FALSE),
                                    ];

                                    // sets detail text to the deconstructed ord_da dictionary
                                    if ($detailText)
                                    {
                                        $detailText .= implode($ord_da, "\t")."\r\n";
                                    }
                                    else
                                    {
                                        $detailText = implode($ord_da, "\t")."\r\n";
                                    }
                                }
                                else
                                {
                                    $validDetailOrder = FALSE;
                                }
                            }
                            else
                            {
                                $validDetailOrder = FALSE;
                            }
                            unset($item);
                        }
                    }
                    else
                    {
                        $validHeaderOrder = FALSE;
                    }
                    // unsets variable to clear memory
                    unset($cust);

                    // makes sure order is valid
                    if ($validHeaderOrder && $validDetailOrder)
                    {
                        // makes sure the path that the text files are to be placed exists if not it creates it.
                        if (file_exists($companyInfo['path'].'\SOORDERS\\'))
                        {
                            if (file_exists($companyInfo['path'].'\SOORDERS\ACTIVE\\'))
                            {
                                // puts files in correct location
                                $ordHeaderFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'.'ord_ha_'.$companyInfo['ord_num'].'.txt', 'w+');
                                fputs($ordHeaderFile, $headerText);
                                $ordDetailFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'."ord_da_".$companyInfo['ord_num'].".txt", 'w+');
                                fputs($ordDetailFile, $detailText);
                            }
                            else
                            {
                                if (mkdir($companyInfo['path'].'\SOORDERS\ACTIVE\\'))
                                {
                                    // puts files in correct location
                                    $ordHeaderFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'.'ord_ha_'.$companyInfo['ord_num'].'.txt', 'w+');
                                    fputs($ordHeaderFile, $headerText);
                                    $ordDetailFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'."ord_da_".$companyInfo['ord_num'].".txt", 'w+');
                                    fputs($ordDetailFile, $detailText);
                                }
                            }
                        }
                        else
                        {
                            if (mkdir($companyInfo['path'].'\SOORDERS\\'))
                            {
                                if (mkdir($companyInfo['path'].'\SOORDERS\ACTIVE\\'))
                                {
                                    // puts files in correct location
                                    $ordHeaderFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'.'ord_ha_'.$companyInfo['ord_num'].'.txt', 'w+');
                                    fputs($ordHeaderFile, $headerText);
                                    $ordDetailFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'."ord_da_".$companyInfo['ord_num'].".txt", 'w+');
                                    fputs($ordDetailFile, $detailText);
                                }
                            }
                        }
                        // unsets more variable to save memory
                        unset($headerText);
                        unset($detailText);
                    }
                    else
                    {
                        // puts it in the postgres database if the order is not valid for any reason
                        $soHeadersTable = TableRegistry::get('SoHeaders');
                        $soHeader = $soHeadersTable->newEntity();

                        $soHeader->cust_num = $ordArray['custNum'];
                        $soHeader->cust_po_num = $ordArray['custPONum'];
                        $soHeader->created = $ordArray['dateCreated'];
                        $soHeader->duedate = $ordArray['dueDate'];
                        $soHeader->address1 = $ordArray['address1'];
                        $soHeader->address2 = $ordArray['address2'];
                        $soHeader->city = $ordArray['city'];
                        $soHeader->email = $ordArray['email'];
                        $soHeader->name = $ordArray['name'];
                        $soHeader->shipadd1 = $ordArray['shipadd1'];
                        $soHeader->shipadd2 = $ordArray['shipadd2'];
                        $soHeader->shipcity = $ordArray['shipcity'];
                        $soHeader->shipcode = $ordArray['shipcode'];
                        $soHeader->shipname = $ordArray['shipname'];
                        $soHeader->shipst = $ordArray['shipst'];
                        $soHeader->shipzip = $ordArray['shipzip'];
                        $soHeader->state = $ordArray['state'];
                        $soHeader->zip = $ordArray['zip'];
                        $soHeader->created_by = $this->Auth->user('id');

                        if($soHeadersTable->save($soHeader))
                        {
                            // headerId is used to link the header and detail records
                            $headerId = $soHeader->id;
                            foreach ($ordArray['Details'] as $detArray)
                            {
                                $soDetailsTable = TableRegistry::get('SoDetails');
                                $soDetail = $soDetailsTable->newEntity();

                                $soDetail->so_header_id = $headerId;
                                $soDetail->item_num = $detArray['itemNum'];
                                $soDetail->item_qty = $detArray['itemQty'];
                                $soDetail->notes = $detArray['notes'];
                                $soDetail->price = $detArray['price'];

                                if($soDetailsTable->save($soDetail))
                                {

                                }
                            }
                        }
                    }
                }
                // returns the counts that are entered
                $return = array("countsReceived" => $ordersReceived);
                echo json_encode($return);
                $this->set("_serialize", $return);
            }
        }
		// resets time out limit
        set_time_limit(60);
        // makes sure that a web page is not rendered
        $this->autorender = FALSE;
    }


    /**
     * Physical Inventory submit method
     *
     * @param inphysst dictionary from ios device.
     * @return the number of counts received.
     * This method just saves the counts into the postgres database.
     */
    public function json_inphysst_submit()
    {
        set_time_limit(5000);
        //
        if ($this->request->is('post'))
        {
            $countsReceived = 0;
            $data = $this->request->input();
            $obj = json_decode($data, true);
            if ($obj['Counts'])
            {
                foreach ($obj['Counts'] as $invArray)
                {
                    $countsReceived++;
                    $inPhysstTable = TableRegistry::get('InPhysst');
                    $inphysst = $inPhysstTable->newEntity();

                    $inphysst->itemnum = $invArray['itemnum'];
                    $inphysst->whcode = $invArray['whcode'];
                    $inphysst->company = $invArray['company'];
                    $inphysst->binloc = $invArray['binloc'];
                    $inphysst->userid = $invArray['userid'];
                    $inphysst->serlotnum = $invArray['serlotnum'];
                    $inphysst->serlot = $invArray['serlot'];
                    $inphysst->physcount = $invArray['physcount'];
                    $inphysst->created_by = $this->Auth->user('id');

                    if($inPhysstTable->save($inphysst))
                    {

                    }
                }
            }
        }

        $return = array("countsReceived" => $countsReceived);
	    echo json_encode($return);
        $this->set("_serialize", $return);

        set_time_limit(60);
        $this->autorender = FALSE;
    }

    public function json_delivery_submit()
    {
        set_time_limit(5000);
        if ($this->request->is('post'))
        {
            $data = $this->request->input();
            $obj = json_decode($data, true);
            if ($obj['Signatures'])
            {
                $company = $this->Auth->user('company_id');
                $this->loadModel('Companies');
                $companyInfo = $this->Companies->get($company);

                foreach ($obj['Signatures'] as $sigArray)
                {
                    $validTrans = $this->order_number_archive_one($sigArray['deliveryNum']);
                    $validArch = $this->order_number_transaction_one($sigArray['deliveryNum']);

                    $filename_path = $companyInfo['path'].'\DELIVERIES\ACTIVE\\'.$sigArray['deliveryNum'].".jpg";
                    $decoded=base64_decode($sigArray['signature']);

                    if (($validTrans || $validArch) && $decoded)
                    {
                        if (file_exists($companyInfo['path'].'\DELIVERIES\\'))
                        {
                            if (file_exists($companyInfo['path'].'\DELIVERIES\ACTIVE\\'))
                            {
                                file_put_contents($filename_path, $decoded);
                            }
                            else
                            {
                                if (mkdir($companyInfo['path'].'\DELIVERIES\ACTIVE\\'))
                                {
                                    file_put_contents($filename_path, $decoded);
                                }
                            }
                        }
                        else
                        {
                            if (mkdir($companyInfo['path'].'\DELIVERIES\\'))
                            {
                                if (mkdir($companyInfo['path'].'\DELIVERIES\ACTIVE\\'))
                                {
                                    file_put_contents($filename_path, $decoded);
                                }
                            }
                        }
                    }
                    else
                    {
                        $soDeliveriesTable = TableRegistry::get('SoDeliveries');
                        $soDelivery = $soDeliveriesTable->newEntity();

                        $soDelivery->delivery_num = $sigArray['deliveryNum'];
                        $soDelivery->note = $sigArray['note'];
                        $soDelivery->name = $sigArray['name'];
                        $soDelivery->signature = (string)$decoded;
                        $soDelivery->date_signed = $sigArray['dateSigned'];
                        $soDelivery->created_by = $this->Auth->user('id');

                        if($soDeliveriesTable->save($soDelivery))
                        {

                        }
                    }
                }
            }
        }
        set_time_limit(60);
        $this->autorender = FALSE;
    }

    public function customer_one($id = null)
    {
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'AR');
        $dd = $this->retrieveDataDictionary("arcustmm", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM arcustmm WHERE number == \"$id\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = $result->GetRowAssoc();
        }
        else
        {
            $return = NULL;
        }
        $this->vfpCloseConnection($vfpConn);
        return $return;
    }

    public function item_one($id = null)
    {
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
        $dd = $this->retrieveDataDictionary("initemmm", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM initemmm WHERE itemnum == \"$id\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = $result->GetRowAssoc();
        }
        else
        {
            $return = NULL;
        }
        $this->vfpCloseConnection($vfpConn);
        return $return;
    }

    public function ship_to_one($shipToId = null, $customerID = null)
    {
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'AR');
        $dd = $this->retrieveDataDictionary("arshipdt", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM arshipdt WHERE shipnum == \"$customerID\" AND shipcode == \"$shipToId\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = TRUE;
        }
        else
        {
            $return = FALSE;
        }
        $this->vfpCloseConnection($vfpConn);
        return $return;
    }

    public function order_number_transaction_one($orderNum = null)
    {
        $orderNumFixed = str_pad(trim($orderNum), 8, " ", STR_PAD_LEFT);
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'SO');
        $dd = $this->retrieveDataDictionary("soordeht", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM soordeht WHERE ordnum == \"$orderNumFixed\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = TRUE;
        }
        else
        {
            $return = FALSE;
        }
        $this->vfpCloseConnection($vfpConn);
        return $return;
    }

    public function order_number_archive_one($orderNum = null)
    {
        $orderNumFixed = str_pad(trim($orderNum), 8, " ", STR_PAD_LEFT);
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'SO');
        $dd = $this->retrieveDataDictionary("soordeha", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM soordeha WHERE ordnum == \"$orderNumFixed\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = TRUE;
        }
        else
        {
            $return = FALSE;
        }
        $this->vfpCloseConnection($vfpConn);
        return $return;
    }

    public function json_companies()
    {
        $companiesTable = TableRegistry::get('Companies');
        $companies = $companiesTables->find('all');
        echo json_encode($companies);
        $this->set("_serialize", $companies);
}
