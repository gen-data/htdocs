<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SoDeliveries Controller
 *
 * @property \App\Model\Table\SoDeliveriesTable $SoDeliveries
 */
class SoDeliveriesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('soDeliveries', $this->paginate($this->SoDeliveries));
        $this->set('_serialize', ['soDeliveries']);
    }

    /**
     * View method
     *
     * @param string|null $id So Delivery id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $soDelivery = $this->SoDeliveries->get($id, [
            'contain' => []
        ]);
        $this->set('soDelivery', $soDelivery);
        $this->set('_serialize', ['soDelivery']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
//    public function add()
//    {
//        $soDelivery = $this->SoDeliveries->newEntity();
//        if ($this->request->is('post')) {
//            $soDelivery = $this->SoDeliveries->patchEntity($soDelivery, $this->request->data);
//            if ($this->SoDeliveries->save($soDelivery)) {
//                $this->Flash->success(__('The so delivery has been saved.'));
//                return $this->redirect(['action' => 'index']);
//            } else {
//                $this->Flash->error(__('The so delivery could not be saved. Please, try again.'));
//            }
//        }
//        $this->set(compact('soDelivery'));
//        $this->set('_serialize', ['soDelivery']);
//    }

    /**
     * Edit method
     *
     * @param string|null $id So Delivery id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $soDelivery = $this->SoDeliveries->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $soDelivery = $this->SoDeliveries->patchEntity($soDelivery, $this->request->data);
            if ($this->SoDeliveries->save($soDelivery)) {
                $this->Flash->success(__('The so delivery has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The so delivery could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('soDelivery'));
        $this->set('_serialize', ['soDelivery']);
    }

    /**
     * Delete method
     *
     * @param string|null $id So Delivery id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $soDelivery = $this->SoDeliveries->get($id);
        if ($this->SoDeliveries->delete($soDelivery)) {
            $this->Flash->success(__('The so delivery has been deleted.'));
        } else {
            $this->Flash->error(__('The so delivery could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function delivery_submit_one($ordNum = NULL)
    {
        $sodelivery = $this->SoDeliveries->get($ordNum);
        
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);

        $validTrans = $this->order_number_archive_one($sodelivery['deliveryNum']);
        $validArch = $this->order_number_transaction_one($sodelivery['deliveryNum']);
        if ($validTrans && $validArch)
        {
            if (file_exists($companyInfo['path'].'\DELIVERIES\\'))
            {
                if (file_exists($companyInfo['path'].'\DELIVERIES\ACTIVE\\'))
                {
                    $filename_path = $sodelivery['deliveryNum'].".jpg";
                    $decoded=base64_decode($sodelivery['signature']);
                    file_put_contents($companyInfo['path'].'\DELIVERIES\ACTIVE\\'.$filename_path, $decoded);
                }
                else
                {
                    if (mkdir($companyInfo['path'].'\DELIVERIES\ACTIVE\\'))
                    {
                        $filename_path = $sodelivery['deliveryNum'].".jpg";
                        $decoded=base64_decode($sodelivery['signature']);
                        file_put_contents($companyInfo['path'].'\DELIVERIES\ACTIVE\\'.$filename_path, $decoded);
                    }
                }
            }
            else
            {
                if (mkdir($companyInfo['path'].'\DELIVERIES\\'))
                {
                    if (mkdir($companyInfo['path'].'\DELIVERIES\ACTIVE\\'))
                    {
                        $$filename_path = $sodelivery['deliveryNum'].".jpg";
                        $decoded=base64_decode($sodelivery['signature']);
                        file_put_contents($companyInfo['path'].'\DELIVERIES\ACTIVE\\'.$filename_path, $decoded);
                    }
                }
            }
            $this->SoHeaders->delete($sodelivery);
            
            $this->Flash->success('The Sales Order has been submitted.');
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            $this->Flash->error('There are still errors in the Delivery.');
            return $this->redirect("/sodeliveries/view/".$ordNum);
        }
    }
    
    public function order_number_transaction_one($orderNum = null)
    {
        $orderNum = str_pad(trim($orderNum), 8, " ", STR_PAD_LEFT);
        echo $orderNum;
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'SO');
        $dd = $this->retrieveDataDictionary("soordeht", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM soordeht WHERE ordnum == \"$orderNum\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = TRUE;
        }
        else
        {
            $return = FALSE;
        }
        return $return;
    }
    
    public function order_number_archive_one($orderNum = null)
    {
        $orderNum = str_pad(trim($orderNum), 8, " ", STR_PAD_LEFT);
        echo $orderNum;
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'SO');
        $dd = $this->retrieveDataDictionary("soordeha", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM soordeha WHERE ordnum == \"$orderNum\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = TRUE;
        }
        else
        {
            $return = FALSE;
        }
        return $return;
    }
}
