<?php
namespace App\Controller;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * SoHeaders Controller
 *
 * @property \App\Model\Table\SoHeadersTable $SoHeaders
 */
class SoHeadersController extends AppController
{
    
    public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->loadModel('SoHeaders');
        $this->loadModel('SoDetails');
        $this->Auth->allow(['add']);
    }
    
    public function isAuthorized($user)
    {
        return true;
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('soHeaders', $this->paginate($this->SoHeaders));
        $this->set('_serialize', ['soHeaders']);
    }

    /**
     * View method
     *
     * @param string|null $id So Header id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $soHeader = $this->SoHeaders->get($id, [
            'contain' => ['SoDetails']
        ]);
        $validItemArray = array();
        $this->set('custNumValid', ((is_null($this->customer_one($soHeader['cust_num']))) ? FALSE : TRUE));
		$this->set('shipToNumValid', $this->ship_to_one($soHeader['shipcode'], $soHeader['cust_num']));
        foreach ($soHeader->so_details as $soDetails)
        {
            if ((is_null($this->item_one($soDetails['item_num']))))
            {
                $validItemArray[$soDetails['item_num']] = FALSE;
            }
            else
            {
                $validItemArray[$soDetails['item_num']] = TRUE;
            }
        }
        $this->set('validItemArray', $validItemArray);
        $this->set('soHeader', $soHeader);
        $this->set('_serialize', ['soHeader']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
//    public function add()
//    {
//        $soHeader = $this->SoHeaders->newEntity();
//        if ($this->request->is('post')) {
//            $soHeader = $this->SoHeaders->patchEntity($soHeader, $this->request->data);
//            if ($this->SoHeaders->save($soHeader)) {
//                $this->Flash->success('The so header has been saved.');
//                return $this->redirect(['action' => 'index']);
//            } else {
//                $this->Flash->error('The so header could not be saved. Please, try again.');
//            }
//        }
//        $this->set(compact('soHeader'));
//        $this->set('_serialize', ['soHeader']);
//    }

    /**
     * Edit method
     *
     * @param string|null $id So Header id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $soHeader = $this->SoHeaders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $soHeader = $this->SoHeaders->patchEntity($soHeader, $this->request->data);
            if ($this->SoHeaders->save($soHeader)) {
                $this->Flash->success('The so header has been saved.');
                return $this->redirect('so_headers/view/'.$soHeader['id']);
            } else {
                $this->Flash->error('The so header could not be saved. Please, try again.');
            }
        }
        $this->set(compact('soHeader'));
        $this->set('_serialize', ['soHeader']);
    }

    /**
     * Delete method
     *
     * @param string|null $id So Header id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $soHeader = $this->SoHeaders->get($id);
        if ($this->SoHeaders->delete($soHeader)) {
            $this->Flash->success('The so header has been deleted.');
        } else {
            $this->Flash->error('The so header could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    public function resubmit_sales_order($ordNum = NULL)
    {
        $soHeader = $this->SoHeaders->get($ordNum,
                ['contain' => ['SoDetails']]);
        
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $validHeaderOrder = TRUE;
        $cust = $this->customer_one($soHeader['cust_num']);
        $ordnum = $companyInfo['ord_num'];
        $ordnum++;
        $companyInfo['ord_num'] = $ordnum;
        $this->Companies->save($companyInfo);
        $detailText = "";
        $headerText = "";
        if (!(is_null($cust)))
        {
            $dueDate = $soHeader['duedate']->format('m/d/Y');
            $ord_ha = [
                'webordnum' => trim($companyInfo['ord_num']),
                'orddate' => "",
                'duedate' => $dueDate,
                'custponum' => trim($soHeader['cust_po_num']),
                'email' => $soHeader['email'],
                'name' => trim($cust['NAME']),
                'address1' => trim($cust['ADDRESS1']),
                'address2' => trim($cust['ADDRESS2']),
                'city' => trim($cust['CITY']),
                'state' => trim($cust['STATE']),
                'zip' => trim($cust['ZIP']),
                'shipname' => ($soHeader['shipcode'] ? "" : $soHeader['shipname']),
                'shipadd1' => ($soHeader['shipcode'] ? "" : $soHeader['shipadd1']),
                'shipadd2' => ($soHeader['shipcode'] ? "" : $soHeader['shipadd2']),
                'shipcity' => ($soHeader['shipcode'] ? "" : $soHeader['shipcity']),
                'shipst' => ($soHeader['shipcode'] ? "" : $soHeader['shipst']),
                'shipzip' => ($soHeader['shipcode'] ? "" : $soHeader['shipzip']),
                'number' => trim($cust['NUMBER']),
                'shipcode' => ($soHeader['shipcode'] ?: ""),
                'ordnum' => ""
            ];
        }
        else
        {
            $validHeaderOrder = FALSE;
        }
        if ($soHeader['so_details'] && $validHeaderOrder)
        {
            if ($headerText)
            {
                $headerText .= implode($ord_ha, "\t")."\r\n";
            }
            else
            {
                $headerText = implode($ord_ha, "\t")."\r\n";
            }
            foreach ($soHeader['so_details'] as $soDetail)
            {
                $validDetailOrder = TRUE;
                if ($soDetail['item_qty'] && $soDetail['item_num'] && $validDetailOrder)
                {
                    $item = $this->item_one($soDetail['item_num']);
                    if (!(is_null($item)) && $validDetailOrder)
                    {
                        $ord_da = [
                            'webordnum' => trim($companyInfo['ord_num']),
                            'itemnum' => trim($item['ITEMNUM']),
                            'notes' => trim($soDetail['notes']),
                            'price' => ($soDetail['price'] ? $soDetail['price'] : $item['STANDPRICE'] * $item['SALESFACT']),
                            'qtyord' => trim($soDetail['item_qty']),
                            'ordnum' => ""
                        ];
                        if ($detailText)
                        {
                            $detailText .= implode($ord_da, "\t")."\r\n";
                        }
                        else
                        {
                            $detailText = implode($ord_da, "\t")."\r\n";
                        }
                    }
                    else
                    {
                        $validDetailOrder = FALSE;
                    }
                }
                else
                {
                    $validDetailOrder = FALSE;
                }
                unset($item);
            }
        }
        else
        {
            $validHeaderOrder = FALSE;
        }
        unset($cust);
        if ($validHeaderOrder && $validDetailOrder)
        {
            if (file_exists($companyInfo['path'].'\SOORDERS\\'))
            {
                if (file_exists($companyInfo['path'].'\SOORDERS\ACTIVE\\'))
                {
                    $ordHeaderFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'.'ord_ha_'.$companyInfo['ord_num'].'.txt', 'w+');
                    fputs($ordHeaderFile, $headerText);
                    $ordDetailFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'."ord_da_".$companyInfo['ord_num'].".txt", 'w+');
                    fputs($ordDetailFile, $detailText);
                }
                else
                {
                    if (mkdir($companyInfo['path'].'\SOORDERS\ACTIVE\\'))
                    {
                        $ordHeaderFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'.'ord_ha_'.$companyInfo['ord_num'].'.txt', 'w+');
                        fputs($ordHeaderFile, $headerText);
                        $ordDetailFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'."ord_da_".$companyInfo['ord_num'].".txt", 'w+');
                        fputs($ordDetailFile, $detailText);
                    }
                }
            }
            else
            {
                if (mkdir($companyInfo['path'].'\SOORDERS\\'))
                {
                    if (mkdir($companyInfo['path'].'\SOORDERS\ACTIVE\\'))
                    {
                        $ordHeaderFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'.'ord_ha_'.$companyInfo['ord_num'].'.txt', 'w+');
                        fputs($ordHeaderFile, $headerText);
                        $ordDetailFile = fopen($companyInfo['path'].'\SOORDERS\ACTIVE\\'."ord_da_".$companyInfo['ord_num'].".txt", 'w+');
                        fputs($ordDetailFile, $detailText);
                    }
                }
            }
            unset($headerText);
            unset($detailText);
            
            foreach ($soHeader['so_details'] as $soDetail)
            {
                $this->SoDetails->delete($soDetail);
            }
            $this->SoHeaders->delete($soHeader);
            
            $this->Flash->success('The Sales Order has been submitted.');
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            $this->Flash->error('There are still errors in the Sales Order.');
            return $this->redirect("/soheaders/view/".$ordNum);
        } 
    }

    public function customer_one($id = null)
    {
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'AR');
        $dd = $this->retrieveDataDictionary("arcustmm", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM arcustmm WHERE number == \"$id\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = $result->GetRowAssoc();
        }
        else
        {
            $return = NULL;
        }
        return $return;
    }

    public function item_one($id = null)
    {
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
        $dd = $this->retrieveDataDictionary("initemmm", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM initemmm WHERE itemnum == \"$id\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = $result->GetRowAssoc();
        }
        else
        {
            $return = NULL;
        }
        return $return;
    }
	
	public function ship_to_one($shipToId = null, $customerID = null)
	{
		$company = $this->Auth->user('company_id');
		$this->loadModel('Companies');
		$companyInfo = $this->Companies->get($company);
		$vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'AR');
		$dd = $this->retrieveDataDictionary("arshipdt", $vfpConn);
		$select = $this->createSelectAllFromVfp($dd);
		$stmt = "SELECT $select FROM arshipdt WHERE shipnum == \"$customerID\" AND shipcode == \"$shipToId\"";
		$result = $this->vfpExecuteSQL($vfpConn, $stmt);
		if ($result->RecordCount() > 0)
		{
			$return = TRUE;
		}
		else
		{
			$return = FALSE;
		}
		return $return;
	}
}
