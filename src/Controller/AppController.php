<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;

include(ROOT.DS.'plugins'.DS.'adodb'.DS.'adodb.inc.php');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    
    public function beforeRender(\Cake\Event\Event $event)
    {
        $this->set('authUser', $this->Auth->user());
        $this->set('userLevel', $this->Auth->user('role_id'));
    }
    
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
//        $this->Security->requireSecure();
    }
    
    public function isAuthorized($user)
    {
        return true;
    }

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {   
        $this->loadComponent('Flash');
        $this->loadComponent('Security');
        $this->loadComponent('Auth', [
            'authorize'=> 'Controller',
            'authenticate' => [
                'Form'
            ],
            'loginRedirect' => [
                'controller' => 'Pages',
                'action' => 'index'
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Pages',
                'action' => 'display'
            ],
            'unauthorizedRedirect' => $this->referer()
        ]);
        
        $this->Auth->allow('none');
    }
    
    public function vfpConnect($path, $company, $db, $debug = FALSE)
    {
        if ($db === NULL)
        {
            
            $dsnvfp = "Driver={Microsoft Visual FoxPro Driver};SourceType=DBC;SourceDB=".trim($path)."\\DBFS\\system\\system".".dbc;Exclusive=No;Deleted=No;Null=Yes";
//            echo $dsnvfp;
            $dbvfp = ADONewConnection('ado');
            $dbvfp->debug = $debug;
            $dbvfp->connect($dsnvfp);// or die('Error connecting to VFP database.');
        }
        else
        {
            $path = $path.'\DBFS\DBFS';
            $dsnvfp = "Driver={Microsoft Visual FoxPro Driver};SourceType=DBC;SourceDB=".trim($path).trim($company)."\\".$db."\\".$db.".dbc;Exclusive=No;";
//            echo $dsnvfp;
            $dbvfp = ADONewConnection('vfp');
            $dbvfp->debug = $debug;
            $dbvfp->connect($dsnvfp);// or die('Error connecting to VFP database.');
        }
        return $dbvfp;
    }
    
    public function vfpExecuteSQL($dbvfp, $SQLStatement)
    {
        return $result = $dbvfp->Execute($SQLStatement);
    }
    
    public function vfpCloseConnection($dbvfp)
    {
        $dbvfp->close();
    }
    
    public function createSelectAllFromVfp($dataDictionary)
    {
        $select = "";
        $x = 1;
        foreach($dataDictionary as $column)
        {
            switch($column->type)
            {
                CASE "C":
                    $fieldType = "character";
                    $select = $select . $column->name . ", ";
                    break;
                CASE "Y":
                    $fieldType = "";
                    break;
                CASE "D":
                    $fieldType = "date";
                    $select = $select . $column->name . ", ";
                    break;
                CASE "T":
                    $fieldType = "";
                    break;
                CASE "B":
                    $fieldType = "bigint";
                    break;
                CASE "F":
                    $fieldType = "bigint";
                    $select = $select . $column->name . ", ";
                    break;
                CASE "G":
                    $fieldType = "";
                    break;
                CASE "I":
                    $fieldType = "int";
                    $select = $select . $column->name . ", ";
                    break;
                CASE "L":
                    $fieldType = "boolean";
                    $select = $select . $column->name . ", ";
                    break;
                CASE "M":
                    $fieldType = "";
                    break;
                CASE "N":
                    $fieldType = "numeric";
                    $select = $select . $column->name . ", ";
                    break;
                CASE "Q":
                    $fieldType = "";
                    break;
                CASE "V":
                    $fieldType = "";
                    break;
                CASE "W":
                    $fieldType = "";
                    break;
                CASE "X":
                    $fieldType = "";
                    break;
            }
            $x++;
        }
        unset($dataDictionary);
        $select1 = trim($select);
        if(substr($select1, -1) === ',')
        {
            $selectNew = substr($select1, 0, -1);
        }
        else
        {
            $selectNew = $select1;
        }
        return $selectNew;
    }
    
    public function retrieveDataDictionary ($TableName, $db)
    {
        return $db->MetaColumns("$TableName");
    }
    
    public function push_notification($deviceToken, $payload = '')
    {
       $apnsHost = 'gateway.sandbox.push.apple.com';
       $apnsPort = 2195;
       $apnsCert = 'apns-dev.pem';
       
       $streamContext = stream_context_create();
       stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
       $apns = stream_socket_client('ssl://'.$apnsHost.':'.$apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
       
       $apnsMessage = chr(0).chr(0).chr(32).pack('H*',str_replace(' ', '', $deviceToken)).chr(0).chr(strlen($payload)).$payload;
       fwrite($apns, $apnsMessage);
       
       socket_close($apns);
       fclose($apns);
    }
}
