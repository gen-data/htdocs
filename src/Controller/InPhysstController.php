<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * InPhysst Controller
 *
 * @property \App\Model\Table\InPhysstTable $InPhysst
 */
class InPhysstController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('inPhysst', $this->paginate($this->InPhysst));
        $this->set('_serialize', ['inPhysst']);
    }

    /**
     * View method
     *
     * @param string|null $id In Physst id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $inPhysst = $this->InPhysst->get($id, [
            'contain' => []
        ]);
        $this->set('inPhysst', $inPhysst);
        $this->set('_serialize', ['inPhysst']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $inPhysst = $this->InPhysst->newEntity();
        if ($this->request->is('post')) {
            $inPhysst = $this->InPhysst->patchEntity($inPhysst, $this->request->data);
            if ($this->InPhysst->save($inPhysst)) {
                $this->Flash->success('The Inventory Count has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The Inventory Count could not be saved. Please, try again.');
            }
        }
        $this->set(compact('inPhysst'));
        $this->set('_serialize', ['inPhysst']);
    }

    /**
     * Edit method
     *
     * @param string|null $id In Physst id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $inPhysst = $this->InPhysst->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $inPhysst = $this->InPhysst->patchEntity($inPhysst, $this->request->data);
            if ($this->InPhysst->save($inPhysst)) {
                $this->Flash->success('The Inventory Count has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The Inventory Count could not be saved. Please, try again.');
            }
        }
        $this->set(compact('inPhysst'));
        $this->set('_serialize', ['inPhysst']);
    }

    /**
     * Delete method
     *
     * @param string|null $id In Physst id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $inPhysst = $this->InPhysst->get($id);
        if ($this->InPhysst->delete($inPhysst)) {
            $this->Flash->success('The Inventory Count has been deleted.');
        } else {
            $this->Flash->error('The Inventory Count could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete Blind method
     *
     * @param string|null $id In Physst id.
     * @returns a bool based on the deletion state.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete_blind($id = null)
    {
        $inPhysst = $this->InPhysst->get($id);
        if ($this->InPhysst->delete($inPhysst)) {
            $result = TRUE;
        } else {
            $result = FALSE;
        }
        return $result;
    }

    /**
     * Update All method
     *
     * @param null.
     * @return void Redirects to index.
     */
    public function update_all()
    {
        $query = $this->InPhysst->find('all');
        $successes = 0;
        $fails = 0;
        foreach($query as $row)
        {
            $binloc = "pribinqty";
            if ($row->binloc !== "")
            {
                $binloc = $this->find_binloc($row);
            }
            $item = $row->itemnum;
            $ware = $row->whcode;
            $user = $row->userid;

            $this->loadModel('Companies');
            $companyInfo = $this->Companies->get($row->company);
            $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');

            $dbArray = [$binloc => $row->physcount];

            $dd = $this->retrieveDataDictionary("inphysst", $vfpConn);
            $select = $this->createSelectAllFromVfp($dd);
            $stmt1 = "SELECT $select FROM inphysst WHERE itemnum LIKE \"$item\" AND whcode LIKE \"$ware\" AND userid LIKE \"$user\"";
            $result1 = $this->vfpExecuteSQL($vfpConn, $stmt1);

            if (gettype($result1) == "boolean")
            {
                $this->Flash->error('Access Denied.  Inventory Worksheet File In Use.');
                return $this->redirect(['action' => 'index']);
            }

            $count = $result1->RecordCount();

            if($row->serlot == 'N')
            {
                if($count > 0 && $vfpConn->AutoExecute("INPHYSST", $dbArray, "UPDATE", "itemnum LIKE \"$item\" AND whcode LIKE \"$ware\" AND userid LIKE \"$user\""))
                {
                    $vfpConn1 = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
                    $return = $this->vfpExecuteSQL($vfpConn1, "SELECT $select FROM inphysst WHERE itemnum LIKE \"$item\" AND whcode LIKE \"$ware\" AND userid LIKE \"$user\"");
                    $physcount = 0;
                    if ($return) {
                        $physcount = $return->GetRowAssoc()['PRIBINQTY'] + $return->GetRowAssoc()['ALT1BINQTY'] + $return->GetRowAssoc()['ALT2BINQTY'];
                    }
                    $dbArray = ['physcount' => $physcount];
                    $vfpConn->AutoExecute("INPHYSST", $dbArray, "UPDATE", "itemnum LIKE \"$item\" AND whcode LIKE \"$ware\" AND userid LIKE \"$user\"");
                    $this->vfpCloseConnection($vfpConn1);

                    $this->delete_blind($row->id);
                    $successes++;
                }
                else
                {
                    $fails++;
                }
            }
            else
            {
                $serlot = $row->serlot;
                if($count > 0 && $vfpConn->AutoExecute("INPHYSST", $dbArray, "UPDATE", "itemnum LIKE \"$item\" AND whcode LIKE \"$ware\" AND userid LIKE \"$user\" AND serlotrcpt LIKE \"$serlot\""))
                {
                    $vfpConn1 = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
                    $return = $this->vfpExecuteSQL($vfpConn1, "SELECT $select FROM whcode LIKE \"$ware\" AND itemnum LIKE \"$item\" AND userid LIKE \"$user\" AND serlotrcpt LIKE \"$serlot\"");
                    $physcount = 0;
                    if ($return) {
                        $physcount = $return->GetRowAssoc()['PRIBINQTY'] + $return->GetRowAssoc()['ALT1BINQTY'] + $return->GetRowAssoc()['ALT2BINQTY'];
                    }
                    $dbArray = ['physcount' => $physcount];
                    $vfpConn1->AutoExecute("INPHYSST", $dbArray, "UPDATE", "whcode LIKE \"$ware\" AND itemnum LIKE \"$item\" AND userid LIKE \"$user\" AND serlotrcpt LIKE \"$serlot\"");
                    $this->vfpCloseConnection($vfpConn1);

                    $this->delete_blind($row->id);
                    $successes++;
                }
                else
                {
                    $fails++;
                }
            }
            $this->vfpCloseConnection($vfpConn);
        }
        if($fails == 0)
        {
            $this->Flash->success('All counts have been inserted into ProfitTool.');
        }
        elseif($successes > 0)
        {
            $this->Flash->success("$successes counts have been inserted into ProfitTool.\n$fails counts could not be inserted into ProfitTool.");
        }
        else
        {
            $this->Flash->error('All counts could not be inserted into ProfitTool.');
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Update method
     *
     * @param string|null $id In Physst id.
     * @return void Redirects to index.
     */
    public function update($id = null)
    {
        $inPhysst = $this->InPhysst->get($id);

        $binloc = "pribinqty";
        if ($inPhysst->binloc !== "")
        {
            $binloc = $this->find_binloc($inPhysst);
        }
        $item = $inPhysst->itemnum;
        $ware = $inPhysst->whcode;
        $user = $inPhysst->userid;

        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($inPhysst->company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
        $dbArray = [$binloc => $inPhysst->physcount];

        $dd = $this->retrieveDataDictionary("inphysst", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt1 = "SELECT $select FROM inphysst WHERE itemnum LIKE \"$item\" AND whcode LIKE \"$ware\" AND userid LIKE \"$user\"";
        $result1 = $this->vfpExecuteSQL($vfpConn, $stmt1);

        if (gettype($result1) == "boolean")
        {
            $this->Flash->error('Access Denied.  Inventory Worksheet File In Use.');
            return $this->redirect(['action' => 'index']);
        }

        $count = $result1->RecordCount();

        if($inPhysst->serlot == 'N')
        {
            if($count > 0 && $vfpConn->AutoExecute("INPHYSST", $dbArray, "UPDATE", "whcode LIKE \"$ware\" AND itemnum LIKE \"$item\" AND userid LIKE \"$user\""))
            {
                $vfpConn1 = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
                $return = $this->vfpExecuteSQL($vfpConn1, "SELECT $select FROM inphysst WHERE itemnum LIKE \"$item\" AND whcode LIKE \"$ware\" AND userid LIKE \"$user\"");
                $physcount = 0;
                if ($return) {
                    $physcount = $return->GetRowAssoc()['PRIBINQTY'] + $return->GetRowAssoc()['ALT1BINQTY'] + $return->GetRowAssoc()['ALT2BINQTY'];
                }
                $dbArray = ['physcount' => $physcount];
                $vfpConn->AutoExecute("INPHYSST", $dbArray, "UPDATE", "itemnum LIKE \"$item\" AND whcode LIKE \"$ware\" AND userid LIKE \"$user\"");
                $this->vfpCloseConnection($vfpConn1);

                $this->delete_blind($inPhysst->id);
                $this->Flash->success('The count has been inserted into ProfitTool.');
            }
            else
            {
                $this->Flash->error('The count could not be inserted into ProfitTool.');
            }
        }
        else
        {
            $serlot = $inPhysst->serlot;
            if($count > 0 && $vfpConn->AutoExecute("INPHYSST", $dbArray, "UPDATE", "whcode LIKE \"$ware\" AND itemnum LIKE \"$item\" AND userid LIKE \"$user\" AND serlotrcpt LIKE \"$serlot\""))
            {
                $vfpConn1 = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
                $return = $this->vfpExecuteSQL($vfpConn1, "SELECT $select FROM whcode LIKE \"$ware\" AND itemnum LIKE \"$item\" AND userid LIKE \"$user\" AND serlotrcpt LIKE \"$serlot\"");
                $physcount = 0;
                if ($return) {
                    $physcount = $return->GetRowAssoc()['PRIBINQTY'] + $return->GetRowAssoc()['ALT1BINQTY'] + $return->GetRowAssoc()['ALT2BINQTY'];
                }
                $dbArray = ['physcount' => $physcount];
                $vfpConn1->AutoExecute("INPHYSST", $dbArray, "UPDATE", "whcode LIKE \"$ware\" AND itemnum LIKE \"$item\" AND userid LIKE \"$user\" AND serlotrcpt LIKE \"$serlot\"");
                $this->vfpCloseConnection($vfpConn1);

                $this->delete_blind($inPhysst->id);
                $this->Flash->success('The count has been inserted into ProfitTool.');
            }
            else
            {
                $this->Flash->error('The count could not be inserted into ProfitTool.');
            }
        }
        $this->vfpCloseConnection($vfpConn);
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Find Bin Location method
     *
     * @param array|null $row In Physst record.
     * @return string with column name for the associated bin location.
     */
    public function find_binloc($row = NULL)
    {
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($row->company);

        $itemnum = $row->itemnum;
        $whnum = $row->whcode;
        $binloc = $row->binloc;

        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
        $dd = $this->retrieveDataDictionary("inwaredt", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt1 = "SELECT $select FROM inwaredt WHERE itemnum == \"$itemnum\" AND whnum == \"$whnum\" AND binloc == \"$binloc\"";
        $stmt2 = "SELECT $select FROM inwaredt WHERE itemnum == \"$itemnum\" AND whnum == \"$whnum\" AND altbinloc == \"$binloc\"";
        $stmt3 = "SELECT $select FROM inwaredt WHERE itemnum == \"$itemnum\" AND whnum == \"$whnum\" AND altbinloc2 == \"$binloc\"";
        $result1 = $this->vfpExecuteSQL($vfpConn, $stmt1);
        $result2 = $this->vfpExecuteSQL($vfpConn, $stmt2);
        $result3 = $this->vfpExecuteSQL($vfpConn, $stmt3);
        if ($result1->RecordCount() > 0)
        {
            $return = "pribinqty";
        }
        elseif ($result2->RecordCount() > 0)
        {
            $return = "alt1binqty";
        }
        elseif ($result3->RecordCount() > 0)
        {
            $return = "alt2binqty";
        }
        else
        {
            $return = FALSE;
        }
        $this->vfpCloseConnection($vfpConn);
        return $return;
    }
}
