<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SoDetails Controller
 *
 * @property \App\Model\Table\SoDetailsTable $SoDetails
 */
class SoDetailsController extends AppController
{
	
    public function isAuthorized($user)
    {
        return true;
    }

    /**
     * Index method
     *
     * @return void
     */
//    public function index()
//    {
//        $this->paginate = [
//            'contain' => ['SoHeaders']
//        ];
//        $this->set('soDetails', $this->paginate($this->SoDetails));
//        $this->set('_serialize', ['soDetails']);
//    }

    /**
     * View method
     *
     * @param string|null $id So Detail id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $soDetail = $this->SoDetails->get($id, [
            'contain' => ['SoHeaders']
        ]);
        $this->set('itemNumValid', ((is_null($this->item_one($soDetail['item_num']))) ? FALSE : TRUE));
        $this->set('soDetail', $soDetail);
        $this->set('_serialize', ['soDetail']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
//    public function add()
//    {
//        $soDetail = $this->SoDetails->newEntity();
//        if ($this->request->is('post')) {
//            $soDetail = $this->SoDetails->patchEntity($soDetail, $this->request->data);
//            if ($this->SoDetails->save($soDetail)) {
//                $this->Flash->success('The so detail has been saved.');
//                return $this->redirect(['action' => 'index']);
//            } else {
//                $this->Flash->error('The so detail could not be saved. Please, try again.');
//            }
//        }
//        $soHeaders = $this->SoDetails->SoHeaders->find('list', ['limit' => 200]);
//        $this->set(compact('soDetail', 'soHeaders'));
//        $this->set('_serialize', ['soDetail']);
//    }

    /**
     * Edit method
     *
     * @param string|null $id So Detail id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $soDetail = $this->SoDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $soDetail = $this->SoDetails->patchEntity($soDetail, $this->request->data);
            if ($this->SoDetails->save($soDetail)) {
                $this->Flash->success('The sales order detail has been saved.');
                return $this->redirect('so_headers/view/'.$soDetail->so_header_id);
            } else {
                $this->Flash->error('The sales order detail could not be saved. Please, try again.');
            }
        }
        $soHeaders = $this->SoDetails->SoHeaders->find('list', ['limit' => 200]);
        $this->set(compact('soDetail', 'soHeaders'));
        $this->set('_serialize', ['soDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id So Detail id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $soDetail = $this->SoDetails->get($id);
        if ($this->SoDetails->delete($soDetail)) {
            $this->Flash->success('The sales order detail has been deleted.');
        } else {
            $this->Flash->error('The sales order detail could not be deleted. Please, try again.');
        }
        return $this->redirect('so_headers/view/'.$soDetail->so_header_id);
    }
	
	public function item_one($id = null)
    {
        $company = $this->Auth->user('company_id');
        $this->loadModel('Companies');
        $companyInfo = $this->Companies->get($company);
        $vfpConn = $this->vfpConnect($companyInfo['path'], $companyInfo['profittool_name'], 'IN');
        $dd = $this->retrieveDataDictionary("initemmm", $vfpConn);
        $select = $this->createSelectAllFromVfp($dd);
        $stmt = "SELECT $select FROM initemmm WHERE itemnum == \"$id\"";
        $result = $this->vfpExecuteSQL($vfpConn, $stmt);
        if ($result->RecordCount() > 0)
        {
            $return = $result->GetRowAssoc();
        }
        else
        {
            $return = NULL;
        }
        return $return;
    }
}
